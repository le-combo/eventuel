
var path;
var stroke = 15;
function onMouseDown(event) {
	// If we produced a path before, deselect it:
	if (path) {
		path.selected = false;
	}

	path = new Path();

  path.style = {
    strokeColor: 'black',
    strokeWidth: stroke
  };
	path.strokeColor = 'black';
}

function onMouseDrag(event) {
	// Every drag event, add a point to the path at the current
	// position of the mouse:
	path.add(event.point);

}

function onMouseUp(event) {
	var segmentCount = path.segments.length;
}
